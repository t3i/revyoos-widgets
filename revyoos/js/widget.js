//Search for params and apply theme and template to BODY
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
var theBody = document.getElementById('revyoos-widget');
if (urlParams.has('theme')) {
  var tema = urlParams.get('theme');
  theBody.className = tema + "-theme";
}
if (urlParams.has('template')) {
  var template = urlParams.get('template');
  theBody.className += " " + template + "-template";
}
if (urlParams.has('color')) {
  var color = "#" + urlParams.get('color');
} else {
  color = "#000000";
}
if (urlParams.has('name')) {
  var name = urlParams.get('name');
} else {
  name = "Your Company/Property Name";
}
//CHANGE CSS COLOR
var i;
var revyoosHolding = document.getElementsByClassName('revyoos-holding');
for (i = 0; i < revyoosHolding.length; i++) {
  revyoosHolding[i].style.backgroundColor = color;
}
var revyoosHoldingName = document.getElementsByClassName('revyoos-holding-name');
for (i = 0; i < revyoosHoldingName.length; i++) {
  revyoosHoldingName[i].style.color = color;
  revyoosHoldingName[i].innerHTML = name;
}
var revyoosCloseBtn = document.getElementsByClassName("revyoos-close-btn");
for (i = 0; i < revyoosCloseBtn.length; i++) {
  revyoosCloseBtn[i].querySelector("svg").style.fill = color;
}
document.getElementById("close-label").style.color = color;



let wSize;
let prevPos;
var posCenter = false;
var theClassList = theBody.classList;
const resizeWidget = (wSize) => {
  //THIS CHANGES THE <BODY> OF THE WIDGET, NOT THE IFRAME
  theClassList.remove('vtab-template');
  theClassList.remove('htab-template');
  theClassList.remove('circle-template');
  theClassList.remove('large-template');
  theClassList.remove('medium-template');
  switch (wSize) {
    case 'medium':
      theClassList.add('medium-template');
      break;
    case 'large':
      theClassList.add('large-template');
      break;
    case 'close':
      //Initial size to close the widget
      if (urlParams.has('closeto')) {
        var template = urlParams.get('closeto');
      }
      theClassList.add(template + "-template");
      break;
  }

  //Search for 'pos-center' to use it later.
  if (urlParams.has('pos') && urlParams.get('pos') == 'center') {
    var posCenter = true;
  }

  //THIS COMMUNICATES WITH THE IFRAME, SENDING SIZE AND POSITION
  window.parent.postMessage({
    prevSize: template,
    prevPos: posCenter,
    newSize: wSize
  }, '*');
  //The second argument of the postMessage method represents
  //the target origin: the hostname of the parent window.
  //This is useful for security, making sure the message is
  //only sent to specific domains but for development purposes,
  //we can set the target origin to '*'. This means any parent
  //window will receive the message.
}

var resize = "large";
if (urlParams.has('otas') && urlParams.get('otas') == 0) {
  resize = "medium";
}
//Si no hay otas, large es medium.
document.getElementById('htab-widget').onclick = () => resizeWidget(resize);
document.getElementById('vtab-widget').onclick = () => resizeWidget(resize);
document.getElementById('circle-widget').onclick = () => resizeWidget(resize);
