window.onmessage = (e) => {
  if (e.data.hasOwnProperty("newSize")) {
    // THIS CHANGES THE IFRAME, NOT THE BODY
    var theIframe = document.getElementById("scp_iframe_general");
    var theClassList = theIframe.classList;
    //Remove all sizes
    theClassList.remove('vtab-template');
    theClassList.remove('htab-template');
    theClassList.remove('circle-template');
    theClassList.remove('large-template');
    theClassList.remove('medium-template');
    //Remove pos-center because medium y large sizes have to be bottom 20px
    theClassList.remove('pos-center');
    switch (e.data.newSize) {
      case 'medium':
        theClassList.add('medium-template');
        break;
      case 'large':
        theClassList.add('large-template');
        break;
      case 'close':
        //Recover original size and position.
        if (e.data.hasOwnProperty("prevSize")) {
          theClassList.add(e.data.prevSize + '-template');
        }
        if (e.data.hasOwnProperty("prevPos") && e.data.prevPos == true) {
          theClassList.add('pos-center');
        }
        break;
    }
  }
};

function resizeIframe(obj) {
  //iframe size automatically fits the content.
  obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
}
