//Widget Object
var w = { //default values
  closeto: "htab",
  template: "htab",
  theme: "light",
  align: "", //default:bottom right
  pos: "",
  name: "Your Company/Property Name",
  color: "000000",
  closed: true,
  otas: []
};

$("#scp_iframe_general").on('load', function() {

  //Close any widget
  $("#scp_iframe_general").contents().find(".revyoos-close-btn").click(function() {
    w.template = w.closeto;
    w.closed = true;
  });

  //Click on HTAB widget to open it
  $("#scp_iframe_general").contents().find("#htab-widget").click(function() {
    w.closed = false;
    w.template = "large";

    //If open with 0 otas, load medium widget
    if (($("input[name='ota']").length) && w.otas.length == 0) {
      w.template = "medium";
      loadWidget();
    }
  });

  //Click on VTAB widget to open it
  $("#scp_iframe_general").contents().find("#vtab-widget").click(function() {
    w.closed = false;
    w.template = "large";

    //If open with 0 otas, load medium widget
    if (($("input[name='ota']").length) && w.otas.length == 0) {
      w.template = "medium";
      loadWidget();
    }
  });

  //Click on Circle widget to open it
  $("#scp_iframe_general").contents().find("#circle-widget").click(function() {
    w.closed = false;
    w.template = "large";

    //If open with 0 otas, load medium widget
    if (($("input[name='ota']").length) && w.otas.length == 0) {
      w.template = "medium";
      loadWidget();
    }
  });

  //On load fill w.otas
  w.otas = [];
  if ($("input[name='ota']").length) {
    $.each($("input[name='ota']:checked"), function() {
      w.otas.push("." + $(this).val());
    });


    //Hide all reviews
    $.each($("#scp_iframe_general").contents().find(".review"), function() {
      $(this).hide();
    });
    //Show all reviews in otas
    if (w.otas.length) {
      $.each(w.otas, function(i, val) {
        $("#scp_iframe_general").contents().find(val).show();
      });
    }
  }

  //Read urlParams
  var urlParams = new URLSearchParams($("#scp_iframe_general").attr("src"));
  //After change widget size when open,
  //this parameter keeps the original size to close the widget
  if (urlParams.has('closeto')) {
    w.closeto = urlParams.get('closeto');
  }
  if (urlParams.has('template')) {
    w.template = urlParams.get('template');
    if (w.template == "embed") {
      w.closed = false;
      $("#revyoos-embed-btn").show();
    } else {
      $("#revyoos-embed-btn").hide();
    }
  }

});

//FORM: Chanage widget template
$(".btn-select-template").click(function() {
  w.template = $("input:radio[name='radioTemplate']:checked").val();

  //In this page, the widget is always aligned to the left, next to the sidebar.
  w.align = "pos-left-settings";

  $("#btn-setup").attr('href', w.template + "-settings.html");
  loadWidget();
});

//FORM: Chanage widget theme
$(".btnradio-theme").click(function() {
  w.theme = $("input:radio[name='radioTheme']:checked").val();
  $("#btn-wbg-" + w.theme).prop('checked', true);
  if (w.closed == true) {
    //keep it closed
    w.template = w.closeto;
  }

  if (w.template == "embed") {
    $("#revyoos-embed-btn").attr("class", "btn-" + w.theme);
  }
  loadWidget();
});

//FORM: Chanage widget alignment
$(".btnradio-position").click(function() {
  w.align = $("input:radio[name='radioPosition']:checked").val();
  if (w.align.indexOf("pos-center") >= 0) {
    w.pos = "center";
  } else {
    w.pos = "";
  }

  if (w.closed == true) {
    //keep it closed
    w.template = w.closeto;
  }

  loadWidget();
});

//FORM: Chenge embed backgroundColor
$(".btnradio-wbg").click(function() {
  $("#scp_iframe_general").contents().find("body").attr("style", "background-color:transparent !important;");

  $("#scp_iframe_general").removeClass("wbg-light");
  $("#scp_iframe_general").removeClass("wbg-dark");
  $("#scp_iframe_general").removeClass("wbg-none");

  $("#scp_iframe_general").addClass($("input:radio[name='radioWbg']:checked").val());

});

//FORM: Change widget color
$("#colorPicker").change(function() {
  w.color = $("#colorPicker").val().substring(1);
  loadWidget();
})

$("#btnDefaultColor").click(function() {
  $("#colorPicker").val("#000000");
  w.color = $("#colorPicker").val().substring(1);
  loadWidget();
})

//FORM: Changes #main background setting a css class.
function changeBackground(bg) {
  $("#main").removeClass().addClass(bg);
}

//FORM: Select OTAs in SOURCE CHECKBOXES
//---------------------------------------------
$("input:checkbox[name='ota']").click(function() {

  var review = "." + $(this).val(); //adds '.' for class name

  if ($(this).prop('checked')) {
    //Display reviews of these otas and add to the otas array
    $("#scp_iframe_general").contents().find(review).show();
    w.otas.push(review);
  } else {
    //Hide reviews of these otas and remove from the otas array
    $("#scp_iframe_general").contents().find(review).hide();
    if (w.otas.indexOf(review) >= 0) {
      w.otas.splice(w.otas.indexOf(review), 1);
    }
  }

  //Are all otas unchecked?
  if (w.otas.length) {

    //Toggle whole list and button
    $("#scp_iframe_general").contents().find(".revyoos-user-reviews").show();
    $("#scp_iframe_general").contents().find(".revyoos-close-btn > span").show();
    $("#scp_iframe_general").contents().find(".load-more-link").show();

    if (w.template != "embed") {

      if (w.closed == true && w.otas.length == 1) {
        w.template = w.closeto;
        loadWidget();
      }

      if (w.closed == false && w.template != "large") {
        w.template = "large";
        loadWidget();
      }

    } else {
      $("#scp_iframe_general").height($("#scp_iframe_general").contents().find("#embed-widget").height());
    }

  } else {
    //All OTAS are unchecked -> Display Medium Template

    if (w.template != "embed") {
      if (w.closed == false) {
        //if open, load medium size
        w.template = "medium";
        loadWidget();
      } else {
        //if closed, recover original template
        w.template = w.closeto;
        loadWidget();
      }

      //Toggle whole list and button
      $("#scp_iframe_general").contents().find(".revyoos-user-reviews").hide();
      $("#scp_iframe_general").contents().find(".revyoos-close-btn > span").hide();
    } else {
      $("#scp_iframe_general").height($("#scp_iframe_general").contents().find("#embed-widget").height());
    }

    $("#scp_iframe_general").contents().find(".load-more-link").hide();

  }

});

//Load Widget when needed
//------------------------------------
function loadWidget() {

  //Clean classes
  $("#scp_iframe_general").removeAttr("class");

  //Add classes to iframe
  if (w.template != "") {
    $("#scp_iframe_general").attr("class", w.template + "-template");

    //Special case when embed
    if (w.template == "embed") {
      $("#scp_iframe_general").attr('onload', 'resizeIframe(this)');
      $("#revyoos-embed-btn").show();
    } else {
      //Clean inline size if not embed
      $("#scp_iframe_general").removeAttr("style");
      $("#scp_iframe_general").removeAttr('onload');
      $("#revyoos-embed-btn").hide();
    }
  } else {
    // Default template
    w.template = "htab";
  }
  if (w.align != "") {
    $("#scp_iframe_general").addClass(w.align);
  }

  //Edit URL params
  var iframeURL = "revyoos/widget.html?";
  if (w.theme == "") {
    w.theme = "light"; //default theme
  }
  iframeURL += "theme=" + w.theme + "&template=" + w.template + "&color=" + w.color + "&closeto=" + w.closeto;
  if (w.pos != '') {
    iframeURL += "&pos=" + w.pos;
  }

  //Add otas if any (index.html does not have this form)
  if ($("input[name='ota']").length) {
    iframeURL += "&otas=" + w.otas.length;
  }

  //Change URL
  $("#scp_iframe_general").attr('src', iframeURL);
}

// Hide tutorial hints after a few seconds
setTimeout(function() {
  $('.hint').fadeOut('slow');
}, 10000); // <-- time in milliseconds
